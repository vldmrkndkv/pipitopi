#!/bin/bash
eth=$(ip -f inet -o addr show eth0|cut -d\  -f 7 | cut -d/ -f 1)
ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

# Get updates
sudo apt-get update
sudo apt-get install unattended-upgrades
sudo systemctl enable unattended-upgrades
sudo systemctl start unattended-upgrades

# Installing pptpd
sudo apt-get install pptpd -y

# edit DNS
sudo echo "ms-dns 1.1.1.1" >> /etc/ppp/pptpd-options
sudo echo "ms-dns 1.0.0.1" >> /etc/ppp/pptpd-options
# iOS
sudo echo "nopcomp" >> /etc/ppp/pptpd-options
sudo echo "noaccomp" >> /etc/ppp/pptpd-options

# Edit PPTP Configuration
echo "Editing PPTP Configuration"
sudo echo "localip 10.0.0.1" >> /etc/pptpd.conf
sudo echo "remoteip 10.0.0.100-200" >> /etc/pptpd.conf

# Enabling IP forwarding in PPTP server
sudo echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sudo sysctl -p

# Edit Default MTU
sudo echo "ifconfig $1 mtu 1492" >> /etc/ppp/ip-up

sudo sed '/logwtmp/d' /etc/pptpd.conf

# Tinkering in Firewall
if [ -z "$eth" ]
	then
		sudo iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE && iptables-save
		sudo iptables --table nat --append POSTROUTING --out-interface ppp0 -j MASQUERADE
		$("sudo iptables -I INPUT -s $ip/8 -i ppp0 -j ACCEPT")
		sudo iptables --append FORWARD --in-interface ens3 -j ACCEPT
	else
		sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE && iptables-save
		sudo iptables --table nat --append POSTROUTING --out-interface ppp0 -j MASQUERADE
		$("sudo iptables -I INPUT -s $ip/8 -i ppp0 -j ACCEPT")
		sudo iptables --append FORWARD --in-interface eth0 -j ACCEPT
fi
iptables -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

# Saving iptables
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
sudo apt install netfilter-persistent iptables-persistent -y

# Restarting Service 
sudo service pptpd restart

# Installing node.js & pm2 & app
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - &&\
sudo apt-get install -y nodejs
npm install -g pm2 express helmet uuid body-parser
wget -O app.js https://gitlab.com/vldmrkndkv/pipitopi/-/raw/main/app.js
pm2 startup
pm2 start app.js
pm2 save

# Installing nginx
sudo apt-get install nginx

